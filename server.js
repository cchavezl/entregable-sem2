require('dotenv').config();
const express = require('express'); //parecido a un import
const body_parser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;  //especifica variable de entorno
const URL_BASE = '/techu/v1/';
const usersFile = require('./users.json');

  app.listen(port,function(){       //Funcion callback
    console.log('NodeJS escuchando en el puerto: ' + port);
    }
  );

  app.use(body_parser.json());  //Parsea y comprueba que lo que llega en el body es un json

//Operación GET (Collection)
  app.get(URL_BASE + 'users',
    function(request,response){  //Función anónima
      response.status(200);  //Código de error
      response.send(usersFile); //La respuesta (send) se ejecuta al final
//      response.status(200).send(usersFile);  //También se puede hacer de esta forma.
    }
  );

//Petición GET a un único usuario mediante ID (instancia)
  app.get(URL_BASE + 'users/:id',  //Un sólo parámetro, el id puede ser cualquier nombre, no solo el nombre del campo
//app.get(URL_BASE + 'users/:id/:p1/:p2',   //Más de un parámetro
    function(request,response){
      console.log(request.params.id);  //Un sólo parámetro
//    console.log('id:' + request.params.id); //Varios parámetros
//    console.log('p1:' + request.params.p1);
//    console.log('p2:' + request.params.p2);
      let pos = request.params.id - 1; //let define variables de forma local, solo para la función
      let respuesta =
            (usersFile[pos] == undefined) ? {"msg": "Usuario no existente"} : usersFile[pos];
      let status_code = (usersFile[pos] == undefined) ? 404 : 200;
      response.status(status_code).send(respuesta);
    }
  );

//Get con query string (req.query)
  app.get(URL_BASE + 'usersq',
    function(req, res){
      console.log(req.query.id);
      console.log(req.query.email);
      let pos = req.params.id - 1;
      let respuesta =
            (usersFile[pos] == undefined) ? {"msg": "Usuario no existente"} : usersFile[pos];
      let status_code = (respuesta == undefined) ? 404 : 200;
      res.status(status_code);
      res.send(usersFile[pos - 1]);
      res.send({"msg": "GET con query"})
    });

//Petición POST a users
  app.post(URL_BASE + 'users',
    function(req, res){
      console.log('POST a users');
      let tam = usersFile.length;   //Devuelve el número de elementos que tiene el array
      let new_user = {
        "id_user": tam + 1,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.email,
        "password": req.body.password
      }
      console.log(new_user);
      usersFile.push(new_user);
      res.send({"msg": "Usuario creado correctamente"});  //Se llama como se haya definido en la función (res)
    }
  );

//Petición DELETE a users (mediante su ID)
    app.delete(URL_BASE + 'users/:id',
      function(req, res){
        console.log('DELETE a users');
        let pos = req.params.id - 1;
        let delete_user = req.body;
        usersFile.splice(pos,1);
        res.send({"msg" : "Usuario eliminado"});
      });

// LOGIN - users.json
//POstman: POST http://localhost:3001/techu/v1/login
      app.post(URL_BASE + 'login',
        function(request, response) {   //callback
          console.log("POST /apicol/v2/login");
          let user = request.body.email;
          let pass = request.body.password;
          for(us of usersFile) {  //recorre el userFile
            if(us.email == user) {
              if(us.password == pass) {
                us.logged = true;
                console.log(user);
                console.log(pass);
                writeUserDataToFile(usersFile);  //Funcion para escribir en userFile
                console.log("Login correcto!");
                response.send({"msg" : "Login correcto.", "idUsuario" : us.id_user, "logged" : "true"});
              }else {
                console.log("Login incorrecto.");
                response.send({"msg" : "Login incorrecto."});
               }
            }
          }
        });

      function writeUserDataToFile(data) {
        var fs = require('fs');
        var jsonUserData = JSON.stringify(data);
        fs.writeFile("./users.json", jsonUserData, "utf8",
        function(err) { //función manejadora para gestionar errores de escritura
          if(err) {
            console.log(err);
          } else {
            console.log("Datos escritos en 'users.json'.");
          }
        })
      };

// LOGOUT - users.json
//Postman: POST http://localhost:3001/techu/v1/logout
      app.post(URL_BASE + 'logout',
        function(request, response) {
          console.log("POST /apicol/v2/logout");
          let userId = request.body.id_user;
          for(us of usersFile) {
            if(us.id_user == userId) {
              if(us.logged) {
                delete us.logged; // borramos propiedad 'logged'
                writeUserDataToFile(usersFile);
                console.log("Logout correcto!");
                response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
              } else {
                console.log("Logout incorrecto.");
                response.send({"msg" : "Logout incorrecto."});
              }
            }  us.logged = true
          }
      });

//Total de usuarios
//Postman: GET http://localhost:3001/techu/v1/total_users
  app.get(URL_BASE + 'total_users',
    function(req, res){
      console.log("POST /apicol/v2/total_users");
      res.status(200);
      res.send({"Número total de usuarios": usersFile.length});
    });
